-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 03, 2014 at 07:42 PM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `customcms`
--
CREATE DATABASE IF NOT EXISTS `customcms` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `customcms`;

-- --------------------------------------------------------

--
-- Table structure for table `lang`
--

CREATE TABLE IF NOT EXISTS `lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang` varchar(60) NOT NULL,
  `label` varchar(100) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `lang`
--

INSERT INTO `lang` (`id`, `lang`, `label`, `value`) VALUES
(1, 'mk', 'page', 'Страна'),
(2, 'en', 'page', 'Page');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `page_id` int(11) NOT NULL,
  `target` enum('_self','_blank') NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `name`, `page_id`, `target`, `active`) VALUES
(1, 'Home', 2, '_self', 1),
(2, 'About us', 3, '_self', 1),
(3, 'Test menu', 2, '_self', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(500) NOT NULL,
  `description` text,
  `body` longtext,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `description`, `body`, `active`, `created`) VALUES
(3, 'about us', 'sdfsdf', '<p>\r\n	sadas adf lkjsdaf lksdjf sdjf sldjf b,jdf hsljdh sjdh sjfh skjdfh kjahblaisj lsjkdn lksjf lkjsf lkdsf lkjsdf klsd fkjs bdalkasj flksd &nbsp;sadas adf lkjsdaf lksdjf sdjf sldjf b,jdf hsljdh sjdh sjfh skjdfh kjahblaisj lsjkdn lksjf lkjsf lkdsf lkjsdf klsd fkjs bdalkasj flksd</p><p>\r\n	sadas adf lkjsdaf lksdjf sdjf sldjf b,jdf hsljdh sjdh sjfh skjdfh kjahblaisj lsjkdn lksjf lkjsf lkdsf lkjsdf klsd fkjs bdalkasj flksd &nbsp;sadas adf lkjsdaf lksdjf sdjf sldjf b,jdf hsljdh sjdh sjfh skjdfh kjahblaisj lsjkdn lksjf lkjsf lkdsf lkjsdf klsd fkjs bdalkasj flksd</p><p>\r\n	sadas adf lkjsdaf lksdjf sdjf sldjf b,jdf hsljdh sjdh sjfh skjdfh kjahblaisj lsjkdn lksjf lkjsf lkdsf lkjsdf klsd fkjs bdalkasj flksd &nbsp;sadas adf lkjsdaf lksdjf sdjf sldjf b,jdf hsljdh sjdh sjfh skjdfh kjahblaisj lsjkdn lksjf lkjsf lkdsf lkjsdf klsd fkjs bdalkasj flksd</p><p>\r\n	sadas adf lkjsdaf lksdjf sdjf sldjf b,jdf hsljdh sjdh sjfh skjdfh kjahblaisj lsjkdn lksjf lkjsf lkdsf lkjsdf klsd fkjs bdalkasj flksd &nbsp;sadas adf lkjsdaf lksdjf sdjf sldjf b,jdf hsljdh sjdh sjfh skjdfh kjahblaisj lsjkdn lksjf lkjsf lkdsf lkjsdf klsd fkjs bdalkasj flksd</p><p>\r\n	sadas adf lkjsdaf lksdjf sdjf sldjf b,jdf hsljdh sjdh sjfh skjdfh kjahblaisj lsjkdn lksjf lkjsf lkdsf lkjsdf klsd fkjs bdalkasj flksd &nbsp;sadas adf lkjsdaf lksdjf sdjf sldjf b,jdf hsljdh sjdh sjfh skjdfh kjahblaisj lsjkdn lksjf lkjsf lkdsf lkjsdf klsd fkjs bdalkasj flksd</p><p>\r\n	sadas adf lkjsdaf lksdjf sdjf sldjf b,jdf hsljdh sjdh sjfh skjdfh kjahblaisj lsjkdn lksjf lkjsf lkdsf lkjsdf klsd fkjs bdalkasj flksd &nbsp;sadas adf lkjsdaf lksdjf sdjf sldjf b,jdf hsljdh sjdh sjfh skjdfh kjahblaisj lsjkdn lksjf lkjsf lkdsf lkjsdf klsd fkjs bdalkasj flksd</p><p>\r\n	sadas adf lkjsdaf lksdjf sdjf sldjf b,jdf hsljdh sjdh sjfh skjdfh kjahblaisj lsjkdn lksjf lkjsf lkdsf lkjsdf klsd fkjs bdalkasj flksd &nbsp;sadas adf lkjsdaf lksdjf sdjf sldjf b,jdf hsljdh sjdh sjfh skjdfh kjahblaisj lsjkdn lksjf lkjsf lkdsf lkjsdf klsd fkjs bdalkasj flksd</p><p>\r\n	sadas adf lkjsdaf lksdjf sdjf sldjf b,jdf hsljdh sjdh sjfh skjdfh kjahblaisj lsjkdn lksjf lkjsf lkdsf lkjsdf klsd fkjs bdalkasj flksd &nbsp;sadas adf lkjsdaf lksdjf sdjf sldjf b,jdf hsljdh sjdh sjfh skjdfh kjahblaisj lsjkdn lksjf lkjsf lkdsf lkjsdf klsd fkjs bdalkasj flksd</p><p>\r\n	sadas adf lkjsdaf lksdjf sdjf sldjf b,jdf hsljdh sjdh sjfh skjdfh kjahblaisj lsjkdn lksjf lkjsf lkdsf lkjsdf klsd fkjs bdalkasj flksd &nbsp;sadas adf lkjsdaf lksdjf sdjf sldjf b,jdf hsljdh sjdh sjfh skjdfh kjahblaisj lsjkdn lksjf lkjsf lkdsf lkjsdf klsd fkjs bdalkasj flksd</p><p>\r\n	sadas adf lkjsdaf lksdjf sdjf sldjf b,jdf hsljdh sjdh sjfh skjdfh kjahblaisj lsjkdn lksjf lkjsf lkdsf lkjsdf klsd fkjs bdalkasj flksd &nbsp;sadas adf lkjsdaf lksdjf sdjf sldjf b,jdf hsljdh sjdh sjfh skjdfh kjahblaisj lsjkdn lksjf lkjsf lkdsf lkjsdf klsd fkjs bdalkasj flksd</p><p>\r\n	sadas adf lkjsdaf lksdjf sdjf sldjf b,jdf hsljdh sjdh sjfh skjdfh kjahblaisj lsjkdn lksjf lkjsf lkdsf lkjsdf klsd fkjs bdalkasj flksd &nbsp;sadas adf lkjsdaf lksdjf sdjf sldjf b,jdf hsljdh sjdh sjfh skjdfh kjahblaisj lsjkdn lksjf lkjsf lkdsf lkjsdf klsd fkjs bdalkasj flksd</p><p>\r\n	sadas adf lkjsdaf lksdjf sdjf sldjf b,jdf hsljdh sjdh sjfh skjdfh kjahblaisj lsjkdn lksjf lkjsf lkdsf lkjsdf klsd fkjs bdalkasj flksd &nbsp;sadas adf lkjsdaf lksdjf sdjf sldjf b,jdf hsljdh sjdh sjfh skjdfh kjahblaisj lsjkdn lksjf lkjsf lkdsf lkjsdf klsd fkjs bdalkasj flksd</p><p>\r\n	sadas adf lkjsdaf lksdjf sdjf sldjf b,jdf hsljdh sjdh sjfh skjdfh kjahblaisj lsjkdn lksjf lkjsf lkdsf lkjsdf klsd fkjs bdalkasj flksd &nbsp;sadas adf lkjsdaf lksdjf sdjf sldjf b,jdf hsljdh sjdh sjfh skjdfh kjahblaisj lsjkdn lksjf lkjsf lkdsf lkjsdf klsd fkjs bdalkasj flksd</p><p>\r\n	sadas adf lkjsdaf lksdjf sdjf sldjf b,jdf hsljdh sjdh sjfh skjdfh kjahblaisj lsjkdn lksjf lkjsf lkdsf lkjsdf klsd fkjs bdalkasj flksd &nbsp;sadas adf lkjsdaf lksdjf sdjf sldjf b,jdf hsljdh sjdh sjfh skjdfh kjahblaisj lsjkdn lksjf lkjsf lkdsf lkjsdf klsd fkjs bdalkasj flksd</p><p>\r\n	sadas adf lkjsdaf lksdjf sdjf sldjf b,jdf hsljdh sjdh sjfh skjdfh kjahblaisj lsjkdn lksjf lkjsf lkdsf lkjsdf klsd fkjs bdalkasj flksd &nbsp;sadas adf lkjsdaf lksdjf sdjf sldjf b,jdf hsljdh sjdh sjfh skjdfh kjahblaisj lsjkdn lksjf lkjsf lkdsf lkjsdf klsd fkjs bdalkasj flksd</p><p>\r\n	sadas adf lkjsdaf lksdjf sdjf sldjf b,jdf hsljdh sjdh sjfh skjdfh kjahblaisj lsjkdn lksjf lkjsf lkdsf lkjsdf klsd fkjs bdalkasj flksd &nbsp;sadas adf lkjsdaf lksdjf sdjf sldjf b,jdf hsljdh sjdh sjfh skjdfh kjahblaisj lsjkdn lksjf lkjsf lkdsf lkjsdf klsd fkjs bdalkasj flksd</p><p>\r\n	sadas adf lkjsdaf lksdjf sdjf sldjf b,jdf hsljdh sjdh sjfh skjdfh kjahblaisj lsjkdn lksjf lkjsf lkdsf lkjsdf klsd fkjs bdalkasj flksd &nbsp;sadas adf lkjsdaf lksdjf sdjf sldjf b,jdf hsljdh sjdh sjfh skjdfh kjahblaisj lsjkdn lksjf lkjsf lkdsf lkjsdf klsd fkjs bdalkasj flksd</p>\r\n', 1, '2014-10-29 12:00:00'),
(2, 'Welcome', 'Welcome to our web site bla bla\r\n', '<p>\r\n	s fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsda</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	f sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf <strong>sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf </strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	fsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf<span style="color:#ff0000;"> sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fs</span>dfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsfs fsdfsdaf sadf sdfsadf sf sfsf</p>\r\n', 1, '2014-10-29 12:27:13');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `value` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `value`) VALUES
(1, 'title', 'Welcome to sitename.com'),
(2, 'footer', 'Copyright 2014 | Sitename.com'),
(3, 'site_name', 'sitename.com');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
