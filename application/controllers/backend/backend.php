<?php
/**
 * Backend Controller
 */
class Backend extends Admin_Controller {
	
	public function __construct() {
		parent::__construct();

		$this->load->library('grocery_CRUD');

	}
	
	public function index()
    {
		$output = new stdClass();
		$this->template->set_layout('_backend_layout');	        
		$this->template->build('backend/index', $output);
    }

	public function users()
	{
		$output = new stdClass();
		$this->template->set_layout('_backend_layout');	        
		$this->template->build('backend/users', $output);
	}		

	public function iframe_users()
	{
		try{
			
			$crud = new grocery_CRUD();
						
			$crud->set_theme('flexigrid');
			$crud->set_subject('user');
			$crud->set_table('users');
			$crud->field_type('password', 'password');
			$crud->required_fields('type','username','password');
			$crud->set_rules('username', 'Username', 'required|min_length[5]|max_length[12]|is_unique[users.username]');
			$crud->set_rules('password', 'Password', 'required|min_length[5]|max_length[50]');
			$crud->callback_before_insert(array($this,'encrypt_password_callback'));
			
			$output = $crud->render();

			$this->load->view('backend/subview', $output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function _password_column_width($value,$row) {
	    return "<span style=\"width:50%; max-width:100px; display:block;\">".$value."</span>";
	}


	public function settings()
	{
		$output = new stdClass();
		$this->template->set_layout('_backend_layout');	        
		$this->template->build('backend/settings', $output);
	}
	
	public function iframe_settings()
	{
		try{

			$crud = new grocery_CRUD();
			
			$crud->set_theme('flexigrid');
			$crud->set_table('settings');
			$crud->display_as('name','');
			$crud->field_type('name', 'readonly');
			$crud->display_as('value','');
			$crud->unset_add();
			$crud->unset_delete();
			
			$output = $crud->render();

			$this->load->view('backend/subview', $output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	public function products()
	{
		$output = new stdClass();
		$this->template->set_layout('_backend_layout');	        
		$this->template->build('backend/products', $output);
	}

	public function iframe_products()
	{
		try{

			$crud = new grocery_CRUD();
			
			$crud->set_theme('flexigrid');
			$crud->set_table('products');
			$crud->display_as('name','');
			$crud->field_type('name', 'readonly');
			$crud->display_as('value','');
			$crud->unset_add();
			$crud->unset_delete();
			
			$output = $crud->render();

			$this->load->view('backend/subview', $output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

}