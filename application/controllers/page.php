<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class page extends CI_Controller{

	function theme($file,$data=array()){
		$this->load->view('../../assets/themes/mbusiness/'.$file,$data);
	}

	public function index(){
		redirect(site_url('page/view/2'));
	}
	
	public function view(){
		$output=array();
		$this->load->model('settings');
		$this->load->model('menu');
		$this->load->model('pagemodel','pagem');
		$output['settings']=$this->settings->get();
		$output['menu']=$this->menu->get();
		$output['page']=$this->pagem->get_by_id($this->uri->segment(3));
		
		$this->theme('page.php',$output);
	}
	
	



}


