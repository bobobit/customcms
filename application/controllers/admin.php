<?php

class admin extends CI_Controller{
	
	public function __construct(){
		parent::__construct();
		
		$this->load->library('session');
		if(!$this->session->userdata('lang')){
			if(@$_GET['lang']){
				$this->session->set_userdata(array('lang'=>@$_GET['lang']));
			}
			else{
				$this->session->set_userdata(array('lang'=>'en'));
			}
		}
		else if(@$_GET['lang']){
			$this->session->set_userdata(array('lang'=>@$_GET['lang']));
		}
		$this->load->library('grocery_CRUD');
		$this->languages=$this->db->query("SELECT * FROM lang WHERE lang='".$this->session->userdata('lang')."'")->result_array();
		@session_start();
		$tmp_lng=array();
		foreach($this->languages as $key=>$value){
			$tmp_lng[$value['label']]=$value['value'];
		}
		$this->languages=$tmp_lng;
		
	}
	
	
	
	public function index(){
			$this->check_access(array('admin','moderator'));
			$crud = new grocery_CRUD();
			$l=$this->languages;
			
			$crud->set_theme('flexigrid');
			$crud->set_table('pages');
			$crud->set_subject($l['page']);
			$crud->columns('title','active','created');
			$crud->set_field_upload('picture','assets/uploads/files');
			$crud->unset_print();
			$crud->unset_edit_fields('created');
			$crud->unset_export();
			$crud->unset_texteditor('description');
			$mmm = $crud->render();

			$this->load->view('example.php',$mmm);
		
	}
	
	public function login(){
		$this->load->view('login.php');
	}
	
	public function validatelogin(){
	
		$logged=$this->db->query("SELECT type FROM users WHERE username='".$_POST['username']."'
		AND  password='".md5($_POST['password'].'x7&j*9kl)(')."' AND type != 'user'")->result_array();
		$_SESSION['user_type']=$logged[0]['type'];
		$logged=count($logged);

		
		if($logged){
			@$_SESSION['logged_in']=true;
		}
		else{
			@$_SESSION['logged_in']=false;
		}
		redirect(site_url('admin'));
	}
	
	public function logout(){
		@$_SESSION['logged_in']=false;
		$this->check_access();
	}
	
	
	private function check_access($arr=array()){
		if(!$_SESSION['logged_in']){
			redirect(site_url('admin/login'));
		}
		if(!in_array($_SESSION['user_type'],$arr)){
			exit('access denied!');
		}
	}
	
	public function media(){
			$this->check_access(array('admin','moderator'));
			$crud = new grocery_CRUD();
			$l=$this->languages;
			
			$crud->set_theme('flexigrid');
			$crud->set_subject('file');
			$crud->set_table('media');
			$crud->set_field_upload('file','assets/uploads/files');
			$crud->required_fields('file');
			$mmm = $crud->render();

			$this->load->view('media.php',$mmm);
	}
	
	
	public function menu(){
			$this->check_access(array('admin'));
			$crud = new grocery_CRUD();
			$l=$this->languages;
			
			$crud->set_theme('flexigrid');
			$crud->set_subject('menu');
			$crud->set_table('menu');
			$crud->display_as('name','Име');
			$crud->display_as('page_id','Страна');
			//
			//$crud->callback_column('page_id',array($this,'_callback_webpage_url'));
			$crud->set_relation('page_id','pages','{title} ({created})');
			$crud->required_fields('name','page_id');
			$mmm = $crud->render();

			$this->load->view('example.php',$mmm);
		
	}
	
	
	public function users(){
			$this->check_access(array('admin'));
			$crud = new grocery_CRUD();
			$l=$this->languages;
			
			$crud->set_theme('flexigrid');
			$crud->set_subject('user');
			$crud->set_table('users');
			$crud->field_type('password', 'password');
			$crud->required_fields('type','username','password');
			$crud->set_rules('username', 'Username', 'required|min_length[5]|max_length[12]|is_unique[users.username]');
			$crud->set_rules('password', 'Password', 'required|min_length[5]|max_length[50]');
			$crud->callback_before_insert(array($this,'encrypt_password_callback'));
			
			$mmm = $crud->render();

			$this->load->view('example.php',$mmm);
		
	}
	
	public function encrypt_password_callback($post_array) {
	  $post_array['password'] = md5($post_array['password'].'x7&j*9kl)(');
	  return $post_array;
	}  
	
	
	public function settings(){
			$this->check_access(array('admin'));
			$crud = new grocery_CRUD();
			
			$crud->set_theme('flexigrid');
			$crud->set_table('settings');
			$crud->display_as('name','');
			$crud->field_type('name', 'readonly');
			$crud->display_as('value','');
			$crud->unset_add();
			$crud->unset_delete();
			$mmm = $crud->render();

			$this->load->view('example.php',$mmm);
		
	}
	/*
	public function _callback_webpage_url($value, $row)
	{
		$pages=$this->db->query("SELECT * FROM pages WHERE id=".$value)->result_array();
		$page=$pages[0];
	    return $page['title']. " <a href='".site_url('admin/index/edit/'.$value)."'>[edit]</a>";
	}
	*/
	
}






