<?php $this->load->view('partials/_header');?>	

<div class="row">

	<div class="col-lg-9">
    	<?=($template['body'] ? $template['body'] : '')?>
    </div>

	<div class="col-lg-3">
		<?php $this->load->view('partials/right_sidebar');?>
	</div>

</div><!-- /row -->  

<?php $this->load->view('partials/_footer');?>