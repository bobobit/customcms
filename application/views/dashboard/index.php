<div class="modal" id="iframeAdmin_loading" style="margin-top: 10%;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">loading please wait... </h4>
      </div>
      <div class="modal-body">
	      <div class="text-center">
	      	<img src="<?php echo site_url('assets/img/ajax-loader-big.gif'); ?>" alt="ajax-loader">
	      </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="col-lg-12">
	<iframe id="iframeAdmin"  name="iframeAdmin"
	style="display: none;"
	frameborder="0"
  width="100%"
  marginheight="0"
  marginwidth="0"
  scrolling="no"
  src=""
  >
	</iframe>
</div>
    