<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	
<?php 
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<style type='text/css'>
body
{
	font-family: Arial;
	font-size: 14px;
}
a {
    color: blue;
    text-decoration: none;
    font-size: 14px;
}
a:hover
{
	text-decoration: underline;
}
</style>
</head>
<body>
<ul>
<?php if($_SESSION['user_type']=='admin'){?>
<li><a href="<?php echo site_url('admin/users');?>">users</a></li>
<?php }?>
<li><a href="<?php echo site_url('admin');?>">Pages</a></li>
<?php if($_SESSION['user_type']=='admin'){?>
<li><a href="<?php echo site_url('admin/menu');?>">Menu</a></li>
<li><a href="<?php echo site_url('admin/settings');?>">Settings</a></li>
<?php }?>
<li><a href="<?php echo site_url('admin/logout');?>">Logout</a></li>
</ul>
<a href="javascript:;" onclick="jQuery('#gallery').slideToggle();">Gallery</a>
<div><iframe id="gallery" style="display:none;" width="100%" height="500" src="<?php echo site_url('admin/media');?>" frameborder="0"></iframe></div>
    <div>
		<?php echo $output; ?>
    </div>
</body>
</html>
