<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public $data = array();
	
	public function __construct(){
	
		parent::__construct();
		$this->data['errors'] = array();

		/*Ova se izvrsuva samo ednas ko ce se startuva browserot odnosno sesijata
		(vo CI config mi e podeseno sesijata da se unisti zo gasenje na browserot)*/
		// $session_singleton = $this->session->userdata('session_singleton');

/*		if (! $session_singleton) {
			$this->session->set_userdata(array(
		       'session_singleton' => TRUE
		   	));
		}*/

	}/*end construct*/
/*END CLASS MY_Controller*/}