<?php
class MY_Model extends CI_Model {
	
	protected $_table_name = '';
	protected $_primary_key = 'id';
	protected $_primary_filter = 'intval';
	protected $_order_by = '';
	public $rules = array();
	protected $_timestamps = FALSE;
	
	function __construct() {
		parent::__construct();



		/*So sekoe novo startuvanve na browserot truncate table current_event*/
/*		$singleton_truncate_table_data = $this->session->userdata('singleton_truncate_table_data');

		if (! $singleton_truncate_table_data) {
			$this->session->set_userdata(array(
		       'singleton_truncate_table_data' => TRUE
		   	));
		   	$this->db->truncate('current_event');
		   	$this->db->truncate('reserved_event');
		}*/
	}
	
	public function array_from_post($fields){
		$data = array();
		foreach ($fields as $field) {
			$data[$field] = $this->input->post($field);
		}
		return $data;
	}
	
	public function get($id = NULL, $single = FALSE){
		
		if ($id != NULL) {
			$filter = $this->_primary_filter;
			$id = $filter($id);
			$this->db->where($this->_primary_key, $id);
			$method = 'row';
		}
		elseif($single == TRUE) {
			$method = 'row';
		}
		else {
			$method = 'result';
		}
		
		if (!count($this->db->ar_orderby)) {
			$this->db->order_by($this->_order_by);
		}
		else{
			// ppr($this->db->ar_orderby);
		}

		return $this->db->get($this->_table_name)->$method();
	}
	
	public function get_column($column){
		$this->db->select($column);
		return $this->get();
	}

	public function get_by($where, $single = FALSE){
		$this->db->where($where);
		return $this->get(NULL, $single);
	}
	
	public function save($data, $id = NULL){

		// Set timestamps
		if ($this->_timestamps == TRUE) {
			$now = date('Y-m-d H:i:s');
			$id || $data['created'] = $now;
			$data['modified'] = $now;
		}
		
		// Insert
		if ($id === NULL) {
			!isset($data[$this->_primary_key]) || $data[$this->_primary_key] = NULL;
			$this->db->set($data);
			$this->db->insert($this->_table_name);
			$id = $this->db->insert_id();
		}
		// Update
		else {
			$filter = $this->_primary_filter;
			$id = $filter($id);
			$this->db->set($data);
			$this->db->where($this->_primary_key, $id);
			$this->db->update($this->_table_name);
		}
		
		return $id;
	}
	
	public function delete($id){

		$filter = $this->_primary_filter;
		$id = $filter($id);
		
		if (!$id) {
			return FALSE;
		}
		$this->db->where($this->_primary_key, $id);
		$this->db->limit(1);
		$this->db->delete($this->_table_name);

	}

	public function last_insert_id(){
		return $this->db->insert_id();
	}

	public function last_query(){
		return $this->db->last_query();
	}

	public function truncate_table(){

		$this->db->truncate($this->_table_name);

	}/*end fun truncate_table*/

	public function count_rows(){
		return $this->db->count_all($this->_table_name);
	}

	/*Iminja na kolonite*/
	public function fields_list(){
		return $this->db->list_fields($this->_table_name);
	}


}